use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/05.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut highest_seat_id: isize = 0;

    for line in reader.lines() {
        let line = line.unwrap();

        let line = line
            .replace('F', "0")
            .replace('B', "1")
            .replace('L', "0")
            .replace('R', "1");

        let seat_id = isize::from_str_radix(&line[..], 2).unwrap();

        if seat_id > highest_seat_id {
            highest_seat_id = seat_id;
        }
    }

    println!("{}", highest_seat_id);
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/05.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut ids: Vec<isize> = Vec::new();

    for line in reader.lines() {
        let line = line.unwrap();

        let line = line
            .replace('F', "0")
            .replace('B', "1")
            .replace('L', "0")
            .replace('R', "1");

        let seat_id = isize::from_str_radix(&line[..], 2).unwrap();

        ids.push(seat_id);
    }
    ids.sort();

    for i in 0..ids.len() - 1 {
        if ids[i] + 2 == ids[i + 1] {
            println!("{}", ids[i] + 1);
            break;
        }
    }
}
