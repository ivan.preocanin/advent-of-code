use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/01.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut results: HashMap<i32, i32> = HashMap::new();

    for line in reader.lines() {
        let number = line.unwrap().parse::<i32>().unwrap();
        let substraction = 2020 - number;

        if results.contains_key(&substraction) {
            println!("{}", number * substraction);
        } else {
            results.insert(number, number);
        }
    }
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/01.txt").expect("File not found");
    let reader = BufReader::new(file);

    let input: Vec<i64> = reader
        .lines()
        .map(|x| x.unwrap().parse().unwrap())
        .collect();

    let mut sums: Vec<(i64, i64)> = Vec::new();

    for i in 0..input.len() {
        for j in 0..input.len() {
            sums.push((input[i], input[j]));
        }
    }

    for sum in sums {
        for number in &input {
            if sum.0 + sum.1 + number == 2020 {
                println!("{}", sum.0 * sum.1 * number);
            }
        }
    }
}
