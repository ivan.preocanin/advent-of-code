use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn find_bags_outside(
    current_bag_name: &str,
    input: &Vec<String>,
    bags: &mut HashSet<String>,
) -> HashSet<String> {
    let current_bag_name = String::from(" ") + current_bag_name;
    for i in input {
        if i.contains(&current_bag_name) {
            let s: Vec<&str> = i.split(' ').collect();
            let bag_name = String::from(s[0]) + " " + s[1];
            find_bags_outside(&bag_name[..], input, bags);
            bags.insert(bag_name);
        }
    }

    bags.clone()
}

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/07.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let input: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();

    let bags: HashSet<String> =
        find_bags_outside("shiny gold", &input, &mut HashSet::<String>::new());

    println!("{}", bags.len());
}

fn find_bags_inside(current_bag_name: &str, input: &HashMap<String, String>) -> usize {
    let mut result = 0;
    let val = input.get(current_bag_name).unwrap();
    if val == "no other bags." {
        return 0;
    }
    let bags_with_quantities: Vec<&str> = val.split(", ").collect();
    for b in bags_with_quantities {
        let s: Vec<&str> = b.split(' ').collect();
        let qty: usize = s[0].parse().unwrap();
        let bag_name = String::from(s[1]) + " " + s[2];
        result += qty + qty * find_bags_inside(&bag_name, input);
    }

    result
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/07.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let input: HashMap<String, String> =
        reader
            .lines()
            .fold(HashMap::<String, String>::new(), |mut acc, l| {
                let l = l.unwrap();
                let s: Vec<&str> = l.split(" bags contain ").collect();
                acc.insert(String::from(s[0]), String::from(s[1]));
                acc
            });

    let bags: usize = find_bags_inside("shiny gold", &input);

    println!("{}", bags);
}
