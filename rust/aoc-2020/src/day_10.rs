use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/10.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut input: Vec<i32> = reader
        .lines()
        .map(|l| l.unwrap().parse().unwrap())
        .collect();

    input.sort();

    let mut joltage = 0;
    let mut diff_1 = 0;
    let mut diff_3 = 0;

    for n in input {
        match n - joltage {
            1 => diff_1 += 1,
            3 => diff_3 += 1,
            _ => (),
        }
        if n <= joltage + 3 {
            joltage = n;
        }
    }
    diff_3 += 1;

    println!("{}", diff_1 * diff_3);
}

fn build_reverse_map(mut input: Vec<i64>) -> HashMap<i64, Vec<i64>> {
    input.reverse();
    let mut graph: HashMap<i64, Vec<i64>> = HashMap::new();

    for i in 0..input.len() {
        let mut adjecent: Vec<i64> = Vec::new();
        for j in i + 1..input.len() {
            if input[i] - input[j] <= 3 {
                adjecent.push(input[j]);
            }
        }
        graph.insert(input[i], adjecent);
    }

    graph
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/10.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut input: Vec<i64> = reader
        .lines()
        .map(|l| l.unwrap().parse().unwrap())
        .collect();

    input.insert(0, 0);
    input.sort();
    input.push(input[input.len() - 1] + 3);

    let reverse_map: HashMap<i64, Vec<i64>> = build_reverse_map(input.clone());

    let mut ways: HashMap<i64, i64> = HashMap::new();

    for i in 0..input.len() {
        ways.insert(input[i], 1);
    }

    for i in 1..input.len() {
        let neighbors: Vec<i64> = reverse_map.get(&input[i]).unwrap().to_vec();
        let mut sum: i64 = 0;
        for n in neighbors {
            sum += ways.get(&n).unwrap();
        }
        ways.insert(input[i], sum);
    }

    println!("{}", ways.get(&input[input.len() - 1]).unwrap());
}
