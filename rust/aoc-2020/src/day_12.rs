use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[derive(Clone, Debug, Copy, PartialEq)]
enum Direction {
    North,
    South,
    East,
    West,
}

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/12.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut current_direction: Direction = Direction::East;
    let mut north_south: isize = 0;
    let mut east_west: isize = 0;
    let compass = [
        Direction::East,
        Direction::South,
        Direction::West,
        Direction::North,
    ];

    for line in reader.lines() {
        let line = line.unwrap();
        let action = &line[..1];
        let units: isize = line[1..].parse::<isize>().unwrap();

        match action {
            "N" => north_south += units,
            "S" => north_south -= units,
            "E" => east_west += units,
            "W" => east_west -= units,
            "R" => {
                let units: usize = units as usize / 90;
                let i = compass
                    .iter()
                    .position(|d| d == &current_direction)
                    .unwrap();
                current_direction = compass[(i + units) % 4].clone();
            }
            "L" => {
                let units: usize = units as usize / 90;
                let reversed_compass: Vec<Direction> = compass.iter().copied().rev().collect();
                let i = reversed_compass
                    .iter()
                    .position(|d| d == &current_direction)
                    .unwrap();
                current_direction = reversed_compass[(i + units) % 4].clone();
            }
            "F" => match current_direction {
                Direction::North => north_south += units,
                Direction::South => north_south -= units,
                Direction::East => east_west += units,
                Direction::West => east_west -= units,
            },
            _ => (),
        }
    }

    println!("{}", north_south.abs() + east_west.abs())
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/12.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut waypoint_y: isize = 1;
    let mut waypoint_x: isize = 10;
    let mut ship_y: isize = 0;
    let mut ship_x: isize = 0;

    for line in reader.lines() {
        let line = line.unwrap();
        let action = &line[..1];
        let units: isize = line[1..].parse::<isize>().unwrap();

        match action {
            "N" => waypoint_y += units,
            "S" => waypoint_y -= units,
            "E" => waypoint_x += units,
            "W" => waypoint_x -= units,
            "L" | "R" => {
                let angle = units as f64;
                let radians = if action == "L" {
                    angle.to_radians()
                } else {
                    angle.to_radians() * -1.0
                };
                let x: f64 = waypoint_x as f64;
                let y: f64 = waypoint_y as f64;
                waypoint_x = (x * radians.cos() - y * radians.sin()).round() as isize;
                waypoint_y = (x * radians.sin() + y * radians.cos()).round() as isize;
            }
            "F" => {
                ship_y += waypoint_y * units;
                ship_x += waypoint_x * units;
            }
            _ => (),
        }
    }

    println!("{}", ship_y.abs() + ship_x.abs());
}
