use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn has_required_fields(lines: &Vec<String>) -> bool {
    if (lines.len() > 7) || ((lines.len() == 7) && !lines.iter().any(|x| x.starts_with("cid"))) {
        return true;
    }

    false
}

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/04.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut valid = 0;

    let mut current_fields: Vec<String> = Vec::new();

    for line in reader.lines() {
        let line = line.unwrap();
        if line == "" {
            if has_required_fields(&current_fields) {
                valid += 1;
            }

            current_fields.clear();
            continue;
        }

        let mut a: Vec<String> = line.split(" ").map(|l| String::from(&l[..3])).collect();
        current_fields.append(&mut a);
    }

    if has_required_fields(&current_fields) {
        valid += 1;
    }

    println!("{}", valid);
}

fn is_valid(passport: &HashMap<String, String>) -> bool {
    if !passport.contains_key("byr")
        || !passport.contains_key("iyr")
        || !passport.contains_key("eyr")
        || !passport.contains_key("hgt")
        || !passport.contains_key("hcl")
        || !passport.contains_key("ecl")
        || !passport.contains_key("pid")
    {
        return false;
    }

    for key in passport.keys() {
        match &key[..] {
            // byr (Birth Year) - four digits; at least 1920 and at most 2002.
            "byr" => {
                let year: i32 = passport.get(key).unwrap()[..].parse().unwrap();
                if !(year >= 1920 && year <= 2002) {
                    return false;
                }
            }
            // iyr (Issue Year) - four digits; at least 2010 and at most 2020.
            "iyr" => {
                let year: i32 = passport.get(key).unwrap()[..].parse().unwrap();
                if !(year >= 2010 && year <= 2020) {
                    return false;
                }
            }
            // eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
            "eyr" => {
                let year: i32 = passport.get(key).unwrap()[..].parse().unwrap();
                if !(year >= 2020 && year <= 2030) {
                    return false;
                }
            }
            // hgt (Height) - a number followed by either cm or in:
            // If cm, the number must be at least 150 and at most 193.
            // If in, the number must be at least 59 and at most 76.
            "hgt" => {
                let height = passport.get(key).unwrap();
                match height.len() {
                    4 => {
                        let uom: &str = &height[2..];
                        if uom != "in" {
                            return false;
                        }
                        let height: i32 = height[..2].parse().unwrap();
                        if !(height >= 59 && height <= 76) {
                            return false;
                        }
                    }
                    5 => {
                        let uom: &str = &height[3..];
                        if uom != "cm" {
                            return false;
                        }
                        let height: i32 = height[..3].parse().unwrap();
                        if !(height >= 150 && height <= 193) {
                            return false;
                        }
                    }
                    _ => return false,
                }
            }
            // hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
            "hcl" => {
                let re = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
                let color = passport.get(key).unwrap();
                if !re.is_match(color) {
                    return false;
                }
            }
            // ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
            "ecl" => {
                let allowed_colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
                let color = passport.get(key).unwrap();
                if !allowed_colors.contains(&&color[..]) {
                    return false;
                }
            }
            // pid (Passport ID) - a nine-digit number, including leading zeroes.
            "pid" => {
                let re = Regex::new(r"^[0-9]{9}$").unwrap();
                let pid = passport.get(key).unwrap();
                if !re.is_match(pid) {
                    return false;
                }
            }
            // cid (Country ID) - ignored, missing or not.
            _ => (),
        }
    }
    true
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/04.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut valid = 0;

    let mut current_fields: HashMap<String, String> = HashMap::new();

    for line in reader.lines() {
        let line = line.unwrap();
        if line == "" {
            if is_valid(&current_fields) {
                valid += 1;
            }
            current_fields.clear();
            continue;
        }

        let key_pairs: Vec<String> = line.split(" ").map(|l| String::from(l)).collect();
        for key_pair in key_pairs {
            let key_pair_vec: Vec<String> = key_pair.split(":").map(|l| String::from(l)).collect();
            let key = key_pair_vec[0].clone();
            let val = key_pair_vec[1].clone();
            &current_fields.insert(key, val);
        }
    }

    if is_valid(&current_fields) {
        valid += 1;
    }

    println!("Valid: {}", valid);
}
