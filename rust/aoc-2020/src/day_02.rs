use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/02.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut counter = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        let input: Vec<&str> = line.split(&['-', ' ', ':'][..]).collect();
        let min = input[0].parse::<usize>().unwrap();
        let max = input[1].parse::<usize>().unwrap();
        let letter = input[2];
        let password = input[4];

        let count = password.matches(&letter).count();

        if count >= min && count <= max {
            counter += 1;
        }
    }
    println!("Number of valid passwords: {}", counter);
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/02.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut counter = 0;

    for line in reader.lines() {
        let line = line.unwrap();
        let input: Vec<&str> = line.split(&['-', ' ', ':'][..]).collect();
        let pos1 = input[0].parse::<usize>().unwrap() - 1;
        let pos2 = input[1].parse::<usize>().unwrap() - 1;
        let letter = input[2].chars().nth(0).unwrap();
        let password = input[4];
        let char1 = password.chars().nth(pos1).unwrap();
        let char2 = password.chars().nth(pos2).unwrap();

        if (char1 == letter) != (char2 == letter) {
            counter += 1;
        }
    }
    println!("Number of valid passwords: {}", counter);
}
