use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn traverse(matrix: &Vec<String>, right: usize, down: usize) -> i64 {
    let length: usize = matrix[0].len();
    let mut position = right;
    let mut trees = 0;

    for i in (down..matrix.len()).step_by(down) {
        if matrix[i].chars().nth(position % length).unwrap() == '#' {
            trees += 1;
        }
        position += right;
    }

    trees
}

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/03.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let matrix: Vec<String> = reader
        .lines()
        .map(|l| l.expect("Could not parse this line!"))
        .collect();

    println!("{}", traverse(&matrix, 3, 1));
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/03.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let matrix: Vec<String> = reader
        .lines()
        .map(|l| l.expect("Could not parse this line!"))
        .collect();

    let result = traverse(&matrix, 1, 1)
        * traverse(&matrix, 3, 1)
        * traverse(&matrix, 5, 1)
        * traverse(&matrix, 7, 1)
        * traverse(&matrix, 1, 2);

    println!("{}", result);
}
