use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn sums(slice: &[i64]) -> HashSet<i64> {
    let mut sums: HashSet<i64> = HashSet::new();
    for i in 0..slice.len() {
        for j in i..slice.len() {
            sums.insert(slice[i] + slice[j]);
        }
    }
    sums
}

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/09.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let numbers: Vec<i64> = reader
        .lines()
        .map(|l| l.unwrap().parse().unwrap())
        .collect();

    for i in 25..numbers.len() {
        let sums = sums(&numbers[i - 25..i]);
        if !sums.contains(&numbers[i]) {
            println!("{}", numbers[i]);
            break;
        }
    }
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/09.txt").expect("File not found!");
    let reader = BufReader::new(file);
    let target: i64 = 31161678;

    let numbers: Vec<i64> = reader
        .lines()
        .map(|l| l.unwrap().parse().unwrap())
        .collect();

    'outer: for i in 0..numbers.len() {
        let mut sum: i64 = 0;
        let mut min: i64 = numbers[i];
        let mut max: i64 = numbers[i];

        for j in i..numbers.len() {
            sum += numbers[j];
            if numbers[j] < min {
                min = numbers[j];
            }
            if numbers[j] > max {
                max = numbers[j];
            }
            if sum == target {
                println!("{}", min + max);
                break 'outer;
            }
        }
    }
}
