use std::collections::HashSet;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/06.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut letters: HashSet<char> = HashSet::new();
    let mut current_group: String = String::new();
    let mut sum: usize = 0;

    for line in reader.lines() {
        let line = line.unwrap();
        if line == "" {
            for char in current_group.chars() {
                letters.insert(char);
            }
            sum += letters.len();
            current_group.clear();
            letters.clear();
            continue;
        }
        current_group.push_str(&line[..]);
    }
    for char in current_group.chars() {
        letters.insert(char);
    }
    sum += letters.len();
    println!("Sum: {}", sum);
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/06.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut sets: Vec<HashSet<char>> = Vec::new();
    let mut sum: usize = 0;

    for line in reader.lines() {
        let line = line.unwrap();
        if line == "" {
            let intersection = sets.iter().fold(sets[0].clone(), |acc, hs| {
                acc.intersection(hs).cloned().collect()
            });
            sum += intersection.len();
            sets.clear();
            continue;
        }
        let mut set: HashSet<char> = HashSet::new();
        for char in line.chars() {
            set.insert(char);
        }
        sets.push(set);
    }
    let intersection = sets.iter().fold(sets[0].clone(), |acc, hs| {
        acc.intersection(hs).cloned().collect()
    });
    sum += intersection.len();

    println!("Sum: {}", sum);
}
