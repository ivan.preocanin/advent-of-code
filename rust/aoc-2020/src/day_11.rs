use itertools::Itertools;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::iter::successors;
use std::iter::Iterator;

type Matrix = Vec<Vec<char>>;

fn should_mutate(
    matrix: &Matrix,
    position: (usize, usize),
    limit: usize,
    steps: Option<usize>,
) -> Option<char> {
    let directions: Vec<(isize, isize)> = (-1..=1)
        .cartesian_product(-1..=1)
        .filter(|&position| position != (0, 0))
        .collect();

    let mut seats: Vec<char> = vec![];
    for direction in directions {
        let mut i: isize = position.0 as isize;
        let mut j: isize = position.1 as isize;
        let mut current_step: usize = 0;

        loop {
            current_step += 1;
            i += direction.0;
            j += direction.1;

            if i < 0 || j < 0 || i >= matrix.len() as isize || j >= matrix[0].len() as isize {
                break;
            }

            if ['L', '#'].contains(&matrix[i as usize][j as usize]) {
                seats.push(matrix[i as usize][j as usize].clone());
                break;
            }

            match steps {
                None => (),
                _ => {
                    if steps.unwrap() == current_step {
                        break;
                    }
                }
            }
        }
    }

    match matrix[position.0][position.1] {
        'L' => {
            if !seats.contains(&'#') {
                return Some('#');
            } else {
                return None;
            }
        }
        '#' => {
            if seats.iter().filter(|p| **p == '#').count() >= limit {
                return Some('L');
            } else {
                return None;
            }
        }
        _ => return None,
    }
}

fn mutate(matrix: &Matrix, limit: usize, steps: Option<usize>) -> Option<Matrix> {
    let mut next_gen: Matrix = vec![vec!['.'; matrix[0].len()]; matrix.len()];
    let mut has_mutated: bool = false;

    for i in 0..matrix.len() {
        for j in 0..matrix[0].len() {
            match should_mutate(&matrix, (i, j), limit, steps) {
                Some('L') => {
                    next_gen[i][j] = 'L';
                    has_mutated = true;
                }
                Some('#') => {
                    next_gen[i][j] = '#';
                    has_mutated = true;
                }
                None => {
                    next_gen[i][j] = matrix[i][j];
                }
                _ => (),
            }
        }
    }

    let result = if has_mutated { Some(next_gen) } else { None };

    result
}

fn count_occupied(matrix: &Matrix) -> usize {
    matrix
        .iter()
        .flat_map(|row| row.iter().filter(|&&c| c == '#'))
        .count()
}

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/11.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let matrix: Matrix = reader
        .lines()
        .map(|l| l.unwrap().chars().collect())
        .collect();

    let last_gen: Matrix = successors(Some(matrix), |m| mutate(m, 4, Some(1)))
        .last()
        .unwrap();

    let number_of_occupied = count_occupied(&last_gen);
    println!("{:?}", number_of_occupied);
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/11.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let matrix: Matrix = reader
        .lines()
        .map(|l| l.unwrap().chars().collect())
        .collect();

    let last_gen: Matrix = successors(Some(matrix), |m| mutate(m, 5, None))
        .last()
        .unwrap();

    let number_of_occupied = count_occupied(&last_gen);
    println!("{:?}", number_of_occupied);
}
