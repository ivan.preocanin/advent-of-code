use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/13.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader
        .lines()
        .map(|l| l.expect("Could not parse line"))
        .collect();
    let earliest = lines[0].parse::<usize>().unwrap();
    let mut ids = lines[1]
        .split(",")
        .filter(|s| s != &"x")
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    ids.sort();

    let latest = earliest + ids[0] - 1;

    for n in earliest..latest {
        for id in &ids {
            if n % id == 0 {
                println!("{}", (n - earliest) * id);
                return ();
            }
        }
    }
}

#[allow(dead_code)]
pub fn part_two() {}
