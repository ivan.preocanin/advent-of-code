use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

#[allow(dead_code)]
pub fn part_one() {
    let file = File::open("src/input/08.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut i: isize = 0;
    let mut accumulator = 0;
    let instructions: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();
    let mut performed_instructions: Vec<bool> = vec![false; instructions.len()];

    while !performed_instructions[i as usize] {
        let s: Vec<&str> = instructions[i as usize].split(' ').collect();
        let op_code = s[0];
        let op_arg: isize = s[1].parse().unwrap();

        performed_instructions[i as usize] = true;

        match &op_code[..] {
            "acc" => {
                accumulator += op_arg;
                i += 1;
            }
            "jmp" => i += op_arg,
            "nop" => i += 1,
            _ => (),
        }
    }

    println!("{}", accumulator);
}

#[allow(dead_code)]
pub fn part_two() {
    let file = File::open("src/input/08.txt").expect("File not found!");
    let reader = BufReader::new(file);

    let mut accumulator = 0;
    let mut instructions: Vec<String> = reader.lines().map(|l| l.unwrap()).collect();

    for i in 0..instructions.len() {
        if instructions[i].starts_with("nop") {
            instructions[i] = String::from("jmp") + &instructions[i][3..];
        } else if instructions[i].starts_with("jmp") {
            instructions[i] = String::from("nop") + &instructions[i][3..];
        }

        let mut performed_instructions: Vec<bool> = vec![false; instructions.len()];
        let mut op_ptr: isize = 0;
        while !performed_instructions[op_ptr as usize] {
            let s: Vec<&str> = instructions[op_ptr as usize].split(' ').collect();
            let op_code = s[0];
            let op_arg: isize = s[1].parse().unwrap();

            performed_instructions[op_ptr as usize] = true;

            match &op_code[..] {
                "acc" => {
                    accumulator += op_arg;
                    op_ptr += 1;
                }
                "jmp" => op_ptr += op_arg,
                "nop" => op_ptr += 1,
                _ => (),
            }
            if (op_ptr as usize) == performed_instructions.len() {
                break;
            }
        }

        if *performed_instructions.last().unwrap() {
            println!("{}", accumulator);
        }
        if instructions[i].starts_with("nop") {
            instructions[i] = String::from("jmp") + &instructions[i][3..];
        } else if instructions[i].starts_with("jmp") {
            instructions[i] = String::from("nop") + &instructions[i][3..];
        }

        accumulator = 0;
    }
}
